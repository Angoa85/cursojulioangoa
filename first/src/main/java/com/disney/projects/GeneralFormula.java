package com.disney.projects;

import java.util.Scanner;

public class GeneralFormula {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter quadratic coefficient");
		int a = scanner.nextInt();
		System.out.println("Enter linear coefficient");
		int b = scanner.nextInt();
		System.out.println("Enter constant");
		int c = scanner.nextInt();
		double disc = Math.pow(b, 2) - 4 * a * c;
		if (a != 0) {
			if (disc < 0) {
				System.out.println("\n" + 
						"It has imaginary roots");
			} else {
				double x1 = (-b + Math.sqrt(disc)) / (2 * a);
				double x2 = (-b - Math.sqrt(disc)) / (2 * a);
				System.out.println("X1 = " + x1 + " X2 = " + x2);
			}
		} else {
			System.out.println("The quadratic coefficient must be different from 0");
		}
	}

}
