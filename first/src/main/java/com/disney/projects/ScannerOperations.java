package com.disney.projects;

import java.util.Scanner;

/**
 * they are class is to show the operation of the operators
 * */

public class ScannerOperations {

	 public static void product(int a, int b) {
	        System.out.println("product = " + (a * b));
	    }
	    
	    public static void divider(int a, int b) {
	        System.out.println("divider = " + (a / b));
	    }
	    
	    
	    public static void pow(int a, int b) { 
	        System.out.println("Pow = " + Math.pow(a, b));
	    }
	    
	    
	    public static void squirt(int a, int b) {
	        System.out.println("root A = " +   Math.sqrt(a));
	        System.out.println("root B = " +   Math.sqrt(b));
	    }

	    public static void main(String[] args) {

	        Scanner scanner = new Scanner(System.in);

	        System.out.println("Give A plaese:");
	        int a = scanner.nextInt();

	        System.out.println("Give B plaese:");
	        int b = scanner.nextInt();

	        product(a, b);
	        divider(a, b);
	        pow(a, b);
	        squirt(a, b);
	        
	    }

	}
